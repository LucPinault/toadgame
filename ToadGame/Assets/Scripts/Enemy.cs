﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int health = 100;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "ArmPunch")
        {
            TakeDamage(50);
            Debug.Log("Punched");
        }
    }

    private void Update()
    {
        if (health <= 0)
        {
            Die();
        }
    }

    void TakeDamage(int damage)
    {
        health -= damage;        
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
