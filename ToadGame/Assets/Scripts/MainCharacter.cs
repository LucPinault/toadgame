﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacter : MonoBehaviour {

    public float speed;     
    public GameObject toad;
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBar healthbar;
    public bool isGrounded = false;
    public GameObject toadPunch;
    public GameObject toadStanding;

    void Start()
    {
        currentHealth = maxHealth;
        healthbar.SetMaxHealth(maxHealth);       
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bird")
        {
            TakeDamage(10);
            Debug.Log("Bird hit you");
        }
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthbar.SetHealth(currentHealth);
    }   

    void Update()
    {

        Jump();
        float h = Input.GetAxisRaw("Horizontal");       
        Vector3 direction = new Vector3(h, 0f).normalized;
        transform.Translate(direction * speed * Time.deltaTime);
       
        if (Input.GetKeyDown(KeyCode.A))
        {
            Flip();                   
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Flip();            
        }

        if (Input.GetButtonDown("Fire1"))
        {
            toadPunch.gameObject.SetActive(true);
            toadStanding.gameObject.SetActive(false);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            toadPunch.gameObject.SetActive(false);
            toadStanding.gameObject.SetActive(true);
        }
    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 7f), ForceMode2D.Impulse);
        }
    }

    private void Flip()
    {       
        toad.gameObject.transform.Rotate(0f, 180f, 0f);
    }   
}
